public class Main {
    public static void main(String[]args) {
        try {
            int[] int_array = {1, 2, 3};
            System.out.println("attempting to get a 4th value");
            System.out.println(int_array[4]);
        } catch (ArrayIndexOutOfBoundsException e)

        {   System.out.println("This is why QA Engineers always have to do boundary testing! The array only has 3 values and you've requested a 4th.");

        }
    }
}
